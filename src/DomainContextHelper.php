<?php

namespace Drupal\ctek_domain_context;

use Drupal\Core\Entity\EntityTypeManagerInterface;

class DomainContextHelper {

  protected $termStorage;

  public function __construct(
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->termStorage = $entityTypeManager->getStorage('taxonomy_term');
  }

  public function getActiveDomainTerm() {
    $terms = $this->termStorage->loadByProperties([
      'vid' => 'ctek_domain_context',
      'field_domains' => \Drupal::request()->getHost(),
    ]);
    return array_pop($terms);
  }

  public function getDomainTerms() {
    return $this->termStorage->loadByProperties([
      'vid' => 'ctek_domain_context',
    ]);
  }

}
