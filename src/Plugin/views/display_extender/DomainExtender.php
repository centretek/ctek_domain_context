<?php

namespace Drupal\ctek_domain_context\Plugin\views\display_extender;

use Drupal\Core\Form\FormStateInterface;
use Drupal\ctek_domain_context\DomainContextHelper;
use Drupal\taxonomy\TermInterface;
use Drupal\views\Plugin\views\display_extender\DisplayExtenderPluginBase;
use Drupal\views\Plugin\views\query\Sql;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @ViewsDisplayExtender(
 *   id = "ctek_domain_context_display_extender",
 *   title = @Translation("Domain context")
 * )
 */
class DomainExtender extends DisplayExtenderPluginBase {

  public static function create(
    ContainerInterface $container,
    array $configuration,
                       $plugin_id,
                       $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('ctek_domain_context.helper')
    );
  }

  protected $domainContextHelper;

  public function __construct(
    array $configuration,
          $plugin_id,
          $plugin_definition,
    DomainContextHelper $domainContextHelper
  ) {
    $this->domainContextHelper = $domainContextHelper;
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * Stores some state booleans to be sure a certain method got called.
   *
   * @var array
   */
  public $testState;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['ctek_domain_context_display_extender'] = ['default' => FALSE];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function optionsSummary(&$categories, &$options) {
    parent::optionsSummary($categories, $options);

    $categories['ctek_domain_context'] = [
      'title' => $this->t('Domain Context'),
      'column' => 'third',
      'build' => [
        '#weight' => -100,
      ],
    ];

    $options['ctek_domain_context_use_context'] = [
      'category' => 'ctek_domain_context',
      'title' => $this->t('Use Domain Context'),
      'value' => ($this->options['ctek_domain_context_use_context'] ?? FALSE) ? $this->t('Yes') : $this->t('No'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    switch ($form_state->get('section')) {
      case 'ctek_domain_context_use_context':
        $form['#title'] .= $this->t('Test option');
        $form['ctek_domain_context_use_context'] = [
          '#title' => $this->t('Use domain context'),
          '#type' => 'checkbox',
          '#default_value' => $this->options['ctek_domain_context_use_context'],
        ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    parent::submitOptionsForm($form, $form_state);
    switch ($form_state->get('section')) {
      case 'ctek_domain_context_use_context':
        $this->options['ctek_domain_context_use_context'] = $form_state->getValue('ctek_domain_context_use_context');
        break;
    }
  }

  public function query() {
    parent::query();
    if (!isset($this->options['ctek_domain_context_use_context']) || isset($this->options['ctek_domain_context_use_context']) && !$this->options['ctek_domain_context_use_context']) {
      return;
    }
    $query = $this->view->getQuery();
    if ($query instanceof Sql) {
      /** @var \Drupal\views\Plugin\views\join\JoinPluginBase $join */
      $join = Views::pluginManager('join')->createInstance('standard', [
        'type' => 'LEFT',
        'table' => 'taxonomy_index',
        'field' => 'nid',
        'left_table' => 'node_field_data',
        'left_field' => 'nid',
        'operator' => '=',
      ]);
      $relationship = $query->addRelationship('ctek_domain_context', $join, 'node_field_data');
      $query->addTable('taxonomy_index', $relationship, $join, 'ctek_domain_context');
      $term = $this->domainContextHelper->getActiveDomainTerm();
      $query->setWhereGroup('OR', 'ctek_domain_context');
      if ($term instanceof TermInterface) {
        $query->addWhere('ctek_domain_context', 'tid', $term->id());
      }
      $query->addWhereExpression('ctek_domain_context', 'ctek_domain_context.nid IS NULL');
    }
  }

}
