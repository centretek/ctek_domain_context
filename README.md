# What?
The Domain Context module does only a few things:
1. Provides a code-defined vocabulary that represents domains used to access the site, as
well as some hook implementations designed to prevent users from inadvertently deleting
the vocabulary.
2. Provides an integration with Views that allows for the filtering of content based on 
the currently active domain.
3. Provides a simple API (DomainContextHelper) for determining the current active domain.

# Why?
The module is designed to allow a single site to serve content for multiple domains, but
does so in a considerably simpler and lighter-weight way than things like the Domain
Access modules.

# How?
Install the module. Upon installation, it will have created a vocabulary called Domain.
You will add domains via this vocabulary. Create a new term and assign it whatever domains
should "trigger" this term. For any content type you wish to filter based on domain, add an
entity reference to this vocabulary. The API for determining current domain is available
programmatically, and the Views integration can be set up by first enabling the display
extender at /admin/structure/views/settings/advanced and checking the Domain Context
display extender. Then, in individual views, under Advanced, just enable Use Domain Context.